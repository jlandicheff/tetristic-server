/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.server.model;

/**
 * A tetris player known to the server.
 *
 * A user may be a host or simply a player joining a game.
 */
public class User {
    /**
     * The username used by the player to connect to the server
     */
    private String name;

    /**
     * The number of permanent lines given to the player for the current game.
     *
     * Reset every time a new game is started.
     */
    private int permanentLines;

    /**
     * Whether or not the user has lost the current game.
     */
    private boolean loser;

    /**
     * Ctor for User
     * @param name the username of the player
     */
    public User(String name) {
        this.name = name;
        this.init();
    }

    /**
     * Initialize the player.
     *
     * Reset the number of permanent lines to 0 and set the player as non-losing.
     */
    public void init() {
        this.permanentLines = 0;
        this.loser = false;
    }

    /**
     * Add a permanent line to the player for the current game
     */
    public void addPermanentLine() {
        this.permanentLines += 1;
    }

    /**
     * Mark the player has eliminated from the current game
     */
    public void lose() {
        this.loser = true;
    }

    /**
     * Getter for name
     * @return the username of the User
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for permanentLines
     * @return the number of permanent lines given to the player for the current game.
     */
    public int getPermanentLines() {
        return permanentLines;
    }

    /**
     * Getter for loser
     * @return whether the player has been eliminated from the current game or not
     */
    public boolean isLoser() {
        return loser;
    }
}
