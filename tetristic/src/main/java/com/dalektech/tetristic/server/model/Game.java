/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.server.model;

import java.util.ArrayList;
import java.util.List;

/**
 * A single instance of a multiplayer Tetris game.
 */
public class Game {

    public static final int DEFAULT_COUNTDOWN = 999;

    /**
     * The countdown to the actual beginning of the game set when the host launches the game
     */
    private static final int START_COUNTDOWN = 6;

    /**
     * The User who created the game
     */
    private User host;

    /**
     * The users who joined the game (the host is excluded from the list)
     */
    private List<User> users;

    /**
     * The datetime of the actual beginning of the game, based on server system time
     */
    private long startTime;

    /**
     * The countdown to the beginning of the game
     */
    private int countdown;

    /**
     * Ctor for game
     * @param host the user creating the game
     */
    public Game(User host) {
        this.host = host;
        this.users = new ArrayList<User>();
        this.startTime = -1;
        this.countdown = DEFAULT_COUNTDOWN;
    }

    /**
     * Start the countdown to the beginning of the game
     */
    public void start() {
        this.startTime = System.currentTimeMillis() + START_COUNTDOWN * 1000;
    }

    /**
     * Add a permanent line to every player but the one with the given username
     * @param username the username of the user giving lines to others
     */
    public void completeLine(String username) {
        if (!this.host.getName().equals(username) && !this.host.isLoser()) {
            this.host.addPermanentLine();
        }

        this.users.stream().filter(u -> !u.getName().equals(username) && !u.isLoser()).forEach(User::addPermanentLine);
    }

    public void tick() {
        if (this.startTime > 0) {
            this.countdown = (int) ((startTime - System.currentTimeMillis()) / 1000);
        }
    }

    /**
     * Add a player to the game
     * @param user the user to be added
     */
    public void addUser(User user) {
        this.users.add(user);
    }

    /**
     * Remove a player from the game
     * @param user the user to be removed
     */
    public void removeUser(User user) {
        this.users.remove(user);
    }

    /**
     * Find a player with a given username
     * @param username the username of the player to be found
     * @return the User with the given username, null if it doesn't exist
     */
    public User findUser(String username) {
        return this.users.stream().filter(u -> u.getName().equals(username)).findFirst().orElse(null);
    }

    /**
     * Getter for host
     * @return the User who created the game
     */
    public User getHost() {
        return host;
    }

    /**
     * Getter for users
     * @return the list of players who joined the game
     */
    public List<User> getUsers() {
        return users;
    }

    public int getCountdown() {
        return this.countdown;
    }


}
