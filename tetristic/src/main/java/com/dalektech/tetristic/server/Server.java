/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.server;

import com.dalektech.tetristic.server.model.Game;
import com.dalektech.tetristic.server.model.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.logging.Logger;

@SpringBootApplication
@RestController
public class Server extends WebMvcConfigurerAdapter {
    private GameEngine gameHandler = new GameEngine();
    public static final String LOGGER_NAME = "Tetristic";

    public static void main(String[] args) {
        SpringApplication.run(Server.class, args);
        Logger.getLogger(LOGGER_NAME).info("Server loaded.");
    }

    @RequestMapping("/create/{username}")
    @ResponseBody
    public Game createGame(@PathVariable final String username) {
        Logger.getLogger(LOGGER_NAME).info(username + " is creating a game");
        return gameHandler.createNewGame(username);
    }

    @RequestMapping("/join/{username}/{host}")
    @ResponseBody
    public Game joinGame(@PathVariable final String username, @PathVariable final String host) {
        Logger.getLogger(LOGGER_NAME).info(username + " wants to join " + host + "'s game");
        return gameHandler.joinGame(username, host);
    }

    @RequestMapping("/game/{host}")
    @ResponseBody
    public Game gameInfo(@PathVariable final String host) {
        Logger.getLogger(LOGGER_NAME).info("Retrieve info for " + host + "'s game");
        return gameHandler.getGame(host);
    }

    @RequestMapping("/start/{host}")
    @ResponseBody
    public Game start(@PathVariable final String host) {
        Logger.getLogger(LOGGER_NAME).info("Initiating countdown for " + host + "'s game");
        return gameHandler.startGame(host);
    }

    @RequestMapping("/giveline/{host}/{username}")
    @ResponseBody
    public Game giveLine(@PathVariable final String host, @PathVariable final String username) {
        Logger.getLogger(LOGGER_NAME).info(host + " gives a line to everyone in " + host + "'s game");
        return gameHandler.giveLine(host, username);
    }

    @RequestMapping("/user/{username}")
    @ResponseBody
    public User userInfo(@PathVariable final String username) {
        Logger.getLogger(LOGGER_NAME).info("Retrieve user " + username);
        return gameHandler.getUser(username);
    }

    @RequestMapping("/lose/{username}")
    @ResponseBody
    public User lose(@PathVariable final String username) {
        return gameHandler.lose(username);
    }
}
