/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.server;

import com.dalektech.tetristic.server.model.Game;
import com.dalektech.tetristic.server.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by illis on 03/02/16.
 */
public class GameEngine {
    /**
     * Current game instances
     */
    private List<Game> games;

    /**
     * Current users
     */
    private List<User> users;

    /**
     * Ctor for GameEngine.
     */
    public GameEngine() {
        this.games = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * Create a new game for players to join
     *
     * If a game has already been created by the user,
     * @param username the username of the player initiating the game
     * @return the current state of the Game
     */
    public Game createNewGame(String username) {
        User user = findOrCreateUser(username);
        user.init();

        Game game = new Game(user);

        // remove any game with the same host
        this.games = this.games.stream()
                .filter(g -> !g.getHost().getName().equals(username))
                .collect(Collectors.toList());

        this.games.add(game);

        Logger.getLogger(Server.LOGGER_NAME).info("Created new game for " + username);
        return game;
    }

    /**
     * Join a game created by a given host
     * @param username the username of the player joining the game
     * @param host the username of the player who initiated the game
     * @return the current state of the Game
     */
    public Game joinGame(String username, String host) {
        Game game = this.games.stream().filter(g -> g.getHost().getName().equals(host)).findFirst().orElse(null);

        if (game != null) {
            User user = findOrCreateUser(username);
            user.init();
            
            game.addUser(user);
            Logger.getLogger(Server.LOGGER_NAME).info(username + " joins " + host + "'s game");
        } else {
            Logger.getLogger(Server.LOGGER_NAME).info(host + " doesn't host any game!");
        }

        return game;
    }

    /**
     * Find a user with the given username. If it doesn't exist, create it.
     * @param username the username for the User to retrieve
     * @return the retrieved/created User
     */
    public User findOrCreateUser(String username) {
        return this.users.stream().filter(u -> u.getName().equals(username)).findFirst().orElse(createUser(username));
    }

    /**
     * Create a new user with a given username
     * @param username the username to give the User
     * @return the created User
     */
    private User createUser(String username) {
        Logger.getLogger(Server.LOGGER_NAME).info("Create user " + username);
        User user = new User(username);
        this.users.add(user);

        return user;
    }

    /**
     * Get the state of the game initiated by the given host
     * @param host the username of the host who initiated the game to look for
     * @return the state of the Game
     */
    public Game getGame(String host) {
        Game game = this.games.stream()
                .filter(g -> g.getHost().getName().toLowerCase().equals(host.toLowerCase()))
                .findFirst()
                .orElse(null);

        if (game != null) {
            game.tick();
        }

        return game;
    }

    /**
     * Get a user with the given username
     * @param username the username of the user to look for
     * @return the User with the given username, null if it doesn't exist
     */
    public User getUser(String username) {
        return this.users.stream()
                .filter(u -> u.getName().toLowerCase().equals(username.toLowerCase()))
                .findFirst()
                .orElse(null);
    }

    /**
     * Start the countdown before the beginning of the game
     * @param host the player hosting the game
     * @return the updated state of the game
     */
    public Game startGame(String host) {
        Game game = getGame(host);

        if (game != null) {
            game.start();
        } else {
            Logger.getLogger(Server.LOGGER_NAME).info("Cannot start a non-existing game!");
        }

        return game;
    }

    /**
     * Give a completed line to opponents
     * @param host the host of the game
     * @param giver the username of the player giving the line
     * @return the updated state of the game
     */
    public Game giveLine(String host, String giver) {
        Game game = getGame(host);

        if (game != null) {
            game.completeLine(giver);
            Logger.getLogger(Server.LOGGER_NAME).info("Giving lines from " + giver + " in " + host + "'s game");
        } else {
            Logger.getLogger(Server.LOGGER_NAME).info("Cannot give line: the game doesn't exist");
        }

        return game;
    }

    /**
     * Declare a user as losing their current game
     * @param username the username of the losing user
     * @return the loser
     */
    public User lose(String username) {
        User user = getUser(username);

        if (user != null) {
            user.lose();
            Logger.getLogger(Server.LOGGER_NAME).info(username + " has lost the game!");
        }

        return user;
    }
}
