# Tetristic server #

Tetristic server is the Spring server to be used with the [Tetristic game](https://bitbucket.org/hamajo/tetristic)

### Contributors ###

* JACQUEMAIN Maxime
* LANDICHEFF Jonathan
* RAHMOUN Hajar

### Licence ###

See *LICENCE* file